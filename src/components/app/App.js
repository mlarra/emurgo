import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Landing from '../landing/Landing';
import About from '../about/About';
import Blog from '../blog/Blog';
import Contact from '../contact/Contact';


class App extends Component {

  render() {
      return (
        <div>
          <BrowserRouter>
          
            <Route exact path="/" render={() => <Landing/>}/>
            <Route exact path="/aboutus" render={() => <About/>}/>
            <Route exact path="/services" render={() => <Blog/>}/>
            <Route exact path="/portfolio" render={() => <Contact/>}/>
            
          </BrowserRouter>

        </div>
      )
  }
}

export default App;