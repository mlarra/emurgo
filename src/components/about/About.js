import React, { Component } from 'react'
import Header from '../landing/Header'
import Footer from '../landing/Footer'

 class About extends Component {
    render() {
        return (
            <div>
                <Header/>
                About
                <Footer/>
            </div>
        )
    }
}
export default About;