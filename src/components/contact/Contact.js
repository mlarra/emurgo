import React, { Component } from 'react'
import Header from '../landing/Header'
import Footer from '../landing/Footer'

 class Contact extends Component {
    render() {
        return (
            <div>
                <Header/>
                Contact
                <Footer/>
            </div>
        )
    }
}
export default Contact;