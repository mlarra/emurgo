import React, { Component } from 'react'
import Logo from '../../assets/EMURGO-logo.svg'
import youtube from '../../assets/youtube.svg'
import './landing0.css';

class Landing0 extends Component {
    render() {
        return (
            <div className="landing0">
                <div className="landing0Title">EMURGO Building <br/>a Global Cardano</div>
                <div className="landing0Des">Test task</div>
                <div className="landing0Des">By <img src={Logo} alt="Emurgo"/></div>
                <div className ="buttons">
                    <div className="button">LEARN MORE</div>
                    <div className="button"> <img src={youtube} alt="Emurgo"/> WATCH THE VIDEO</div>
                </div>
            </div>
        )
    }
}

export default Landing0;