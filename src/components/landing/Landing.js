import React, { Component } from 'react'
import Header from './Header'
import Landing0 from './Landing0'
import Landing1 from './Landing1'
import Landing2 from './Landing2'
import Landing3 from './Landing3'
import Landing4 from './Landing4'
import Footer from './Footer'


 class Landing extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Landing0/>
                <Landing1/>
                <Landing2/>
                <Landing3/>
                <Landing4/>
                <Footer/>

            </div>
        )
    }
}

export default Landing;
