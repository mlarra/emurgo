import React, { Component } from 'react'
import Logo from '../../assets/EMURGOTEST-logo.svg'

import './header.css';

 class Header extends Component {
    render() {
        return (
            <div className="mainMenu">
                <div className = "headerLogo">
                    <img src={Logo} alt="Emurgo Logo"/>    
                </div>
                
                <div className="headerTitles">
                    <div className="headerTitle"> About </div>
                    <div className="headerTitle"> Blog </div>
                    <div className="headerTitle"> Contact </div>
                    <div className="headerTitle"> Eng </div>
                </div>


                
            </div>
        )
    }
}

export default Header;

