import React, { Component } from 'react'
import Blog1 from '../../assets/Blog 1.jpg';
import Blog2 from '../../assets/Blog 2.jpg';
import Blog3 from '../../assets/Blog 3.jpg';

import './landing3.css';

class Landing3 extends Component {
    render() {
        return (
            <div className="landing3">
                <div className="sectionTitle"> Blog</div>
                <div className="sectionDesc"> Check out below for our lastest annoucements,<br/>meetups, news stories and press releases</div>
                <div className="blogCards">
                    <div className="card">
                        <div className="cardImg">
                            <img src={Blog1} alt="Emurgo"/>
                        </div>

                        <div className="cardDate">17/06/2019</div>
                        <div className="cardTitle">Met up......</div>

                    </div>
                    <div className="card">
                        <div className="cardImg">
                            <img src={Blog3} alt="Emurgo"/>
                        </div>

                        <div className="cardDate">17/06/2019</div>
                        <div className="cardTitle">Met up......</div>

                    </div>
                    <div className="card">
                        <div className="cardImg">
                            <img src={Blog2} alt="Emurgo"/>
                        </div>

                        <div className="cardDate">17/06/2019</div>
                        <div className="cardTitle">Met up......</div>

                    </div>
                </div>
            </div>
        )
    }
}

export default Landing3;