import React, { Component } from 'react'
import Emurgo from '../../assets/EMURGO.png'
import Input from '../../assets/input-output.png'
import Vacum from '../../assets/vacumlabs.png'
import Cardano from '../../assets/Cardano.png'
import './landing2.css';

class Landing2 extends Component {
    render() {
        return (
            <div className="landing2">
                <div className="landing2Back"></div>
                <div className="landing2Grey">
                     <div className="sectionTitle"> Our collaborators</div>
                     <div className="colla"><img src={Emurgo} alt="Emurgo"/><img src={Input} alt="Emurgo"/><img src={Vacum} alt="Emurgo" /><img src={Cardano} alt="Cardano" /></div>
                </div>
                
            </div>
        )
    }
}

export default Landing2;