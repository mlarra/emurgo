import React, { Component } from 'react';
import Secure from '../../assets/Secure.svg'
import Fast from '../../assets/Fast.svg'
import Simple from '../../assets/Simple.svg'
import './landing1.css';

class Landing1 extends Component {
    render() {
        return (
            <div className="landing1">
                <div className="sectionTitle"> About</div>
                <div className="about"> 
                    <div className="aboutText">
                    <div className="aboutTitle"> Secure </div>
                    <div className="aboutSub"> Our top Priority</div>
                    <div className="aboutDes"> High-quality code, thoroughly tested, security audited and more to make sure that Yoroi works flawlessly. Private keys are encrypted and never shared with our servers or third party providers. In order to preserve your privacy, Yoroi wallet does not even implement analytics.</div>
                    </div>
                    <div className="aboutImg"><img src={Secure} alt="Emurgo" /></div>
                </div>

                <div className="about"> 
                    <div className="aboutImg"><img src={Fast} alt="Emurgo"/></div>
                    <div className="aboutText">
                    <div className="aboutTitle"> Fast </div>
                    <div className="aboutSub"> Our innovation</div>
                    <div className="aboutDes"> Yoroi is a light wallet for Cardano. There is no need to download the blockchain when you open the wallet. So you are ready to send and receive transactions right away.</div>
                    </div>
                    
                </div>

                <div className="about"> 
                    <div className="aboutText">
                    <div className="aboutTitle"> Simple </div>
                    <div className="aboutSub"> Our passion</div>
                    <div className="aboutDes"> We believe that software should have a simple structure and a beautiful user interface. Yoroi has been carefully designed and constructed to offer a great user experience.</div>
                    </div>
                    <div className="aboutImg"><img src={Simple} alt="Emurgo"/></div>
                </div>
                
                
            </div>
        )
    }
}

export default Landing1;