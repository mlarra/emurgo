import React, { Component } from 'react'
import Header from '../landing/Header'
import Footer from '../landing/Footer'

 class Blog extends Component {
    render() {
        return (
            <div>
                <Header/>
                Blog
                <Footer/>
            </div>
        )
    }
}
export default Blog;
